package com.gturedi.views;

import ohos.agp.animation.Animator;

/**
 * simple class to reduce callback hell
 */
@SuppressWarnings("PMD.UncommentedEmptyMethodBody")
public class CustomAnimationListener implements Animator.StateChangedListener {

    @Override
    public void onStart(Animator animator) {

    }

    @Override
    public void onStop(Animator animator) {

    }

    @Override
    public void onCancel(Animator animator) {

    }

    @Override
    public void onEnd(Animator animator) {

    }

    @Override
    public void onPause(Animator animator) {

    }

    @Override
    public void onResume(Animator animator) {

    }

}

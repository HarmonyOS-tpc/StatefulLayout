package com.gturedi.views;

import ohos.agp.components.Component;

import java.io.Serializable;

/**
 * Model builder class to show custom state
 *
 * @see com.gturedi.views.StatefulLayout#showCustom(CustomStateOptions)
 */
@SuppressWarnings("PMD.AvoidFieldNameMatchingMethodName")
public class CustomStateOptions implements Serializable {

    private int imageRes;
    private boolean isLoading;
    private String message;
    private String buttonText;
    private Component.ClickedListener buttonClickListener;

    /**
     * 设置自定义状态的图片资源id
     *
     * @param imageRes 图片的资源id
     * @return 自定义状态对象本身
     */
    public CustomStateOptions image(int imageRes) {
        this.imageRes = imageRes;
        return this;
    }

    /**
     * 设置自定义状态的文本
     *
     * @param val 文本
     * @return 自定义状态对象本身
     */
    public CustomStateOptions message(String val) {
        message = val;
        return this;
    }

    /**
     * 设置自定义状态的按钮文本
     *
     * @param val 按钮文本
     * @return 自定义状态对象本身
     */
    public CustomStateOptions buttonText(String val) {
        buttonText = val;
        return this;
    }

    /**
     * 设置自定义状态的按钮点击事件监听器
     *
     * @param val 按钮点击事件监听器
     * @return 自定义状态对象本身
     */
    public CustomStateOptions buttonClickListener(Component.ClickedListener val) {
        buttonClickListener = val;
        return this;
    }

    /**
     * 显示此自定义状态
     *
     * @return 自定义状态对象本身
     */
    public CustomStateOptions loading() {
        isLoading = true;
        return this;
    }

    /**
     * 获取自定义状态的图片资源id
     *
     * @return 自定义状态的图片资源id
     */
    public int getImageRes() {
        return imageRes;
    }

    /**
     * 获取自定义状态的文本
     *
     * @return 自定义状态的文本
     */
    public String getMessage() {
        return message;
    }

    /**
     * 获取自定义状态的按钮文本
     *
     * @return 自定义状态的按钮文本
     */
    public String getButtonText() {
        return buttonText;
    }

    /**
     * 获取自定义状态的按钮点击事件监听器
     *
     * @return 自定义状态的按钮点击事件监听器
     */
    public Component.ClickedListener getClickListener() {
        return buttonClickListener;
    }

    /**
     * 此自定义状态是否显示中
     *
     * @return 是否显示中
     */
    public boolean isLoading() {
        return isLoading;
    }

}

/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gturedi.app;

import com.gturedi.views.CustomStateOptions;
import com.gturedi.views.StatefulLayout;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.TextAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

/**
 * StatefulLayout演示
 */
public class MainAbility extends Ability implements Component.ClickedListener {

    private StatefulLayout stateful;
    private Component.ClickedListener clickedListener;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);
        clickedListener = new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                showToast(MainAbility.this, "click!");
            }
        };
        stateful = (StatefulLayout) findComponentById(ResourceTable.Id_stateful);
        stateful.setInAnimation(new AnimatorProperty().alphaFrom(0.0f).alpha(1.0f).moveFromX(-getWindowWdith(this) * 0.5f).moveToX(0).setDuration(400));
        stateful.setOutAnimation(new AnimatorProperty().alphaFrom(1.0f).alpha(0.0f).moveFromX(0).moveToX(getWindowWdith(this) * 0.5f).setDuration(400));
        findComponentById(ResourceTable.Id_btn_content).setClickedListener(this);
        findComponentById(ResourceTable.Id_btn_loading).setClickedListener(this);
        findComponentById(ResourceTable.Id_btn_empty).setClickedListener(this);
        findComponentById(ResourceTable.Id_btn_error).setClickedListener(this);
        findComponentById(ResourceTable.Id_btn_offline).setClickedListener(this);
        findComponentById(ResourceTable.Id_btn_locationOff).setClickedListener(this);
        findComponentById(ResourceTable.Id_btn_custom).setClickedListener(this);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_btn_content:
                stateful.showContent();
                break;
            case ResourceTable.Id_btn_loading:
                stateful.showLoading();
                break;
            case ResourceTable.Id_btn_empty:
                stateful.showEmpty();
                break;
            case ResourceTable.Id_btn_error:
                stateful.showError(clickedListener);
                break;
            case ResourceTable.Id_btn_offline:
                stateful.showOffline(clickedListener);
                break;
            case ResourceTable.Id_btn_locationOff:
                stateful.showLocationOff(clickedListener);
                break;
            case ResourceTable.Id_btn_custom:
                stateful.showCustom(new CustomStateOptions()
                        .image(ResourceTable.Graphic_ic_bluetooth_disabled_black_24dp)
                        .message("please open bluetooth")
                        .buttonText("SETTINGS")
                        .buttonClickListener(clickedListener));
                break;
            default:
                break;
        }
    }

    private void showToast(Context abilityContext, String str) {
        Text text = new Text(abilityContext);
        text.setWidth(ComponentContainer.LayoutConfig.MATCH_PARENT);
        text.setHeight(ComponentContainer.LayoutConfig.MATCH_CONTENT);
        text.setTextSize(48);
        text.setText(str);
        text.setMultipleLine(true);
        text.setTextAlignment(TextAlignment.CENTER);
        DirectionalLayout directionalLayout = new DirectionalLayout(abilityContext);
        directionalLayout.setBackground(createDrawable(0xF0F0F0F0, 50));
        DirectionalLayout.LayoutConfig layoutConfig = new DirectionalLayout.LayoutConfig
                (DirectionalLayout.LayoutConfig.MATCH_PARENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        layoutConfig.setMarginBottom(100);
        directionalLayout.setLayoutConfig(layoutConfig);
        directionalLayout.setPadding(20, 30, 20, 30);
        directionalLayout.addComponent(text);
        ToastDialog toastDialog = new ToastDialog(abilityContext);
        toastDialog.setContentCustomComponent(directionalLayout).setDuration(2000).setAutoClosable(true)
                .setAlignment(LayoutAlignment.BOTTOM).setTransparent(true).show();
    }

    private ShapeElement createDrawable(int color, float radius) {
        ShapeElement drawable = new ShapeElement();
        drawable.setShape(ShapeElement.RECTANGLE);
        drawable.setRgbColor(new RgbColor(color));
        drawable.setCornerRadius(radius);
        return drawable;
    }

    private int getWindowWdith(Context context) {
        return context.getResourceManager().getDeviceCapability().width * context.getResourceManager().getDeviceCapability().screenDensity / 160;
    }

}

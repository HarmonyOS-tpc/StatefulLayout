﻿## StatefulLayout
show most common state templates like loading, empty, error etc. To do that all you need to is wrap the target area(view) with StatefulLayout

## Preview
<img src="https://gitee.com/HarmonyOS-tpc/StatefulLayout/raw/master/sample.gif" width="40%"/>

## Gradle
```
allprojects{
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:StatefulLayout:1.0.2'
```

## Usage
Then wrap a view which target area(view) to show states with StatefulLayout
```
  <com.gturedi.views.StatefulLayout
        ohos:id="$+id:stateful"
        ohos:height="match_parent"
        ohos:width="match_parent"
        ohos:stfAnimationEnabled="true"
        >
    
        <!-- your content here  -->
        <DirectionalLayout
            ohos:id="$+id:target"
            ohos:height="match_content"
            ohos:width="match_parent"
            >
            ...
        </DirectionalLayout>

    </com.gturedi.views.StatefulLayout>
```

Finally in your ability/abilityslice get StatefulLayout reference and call showXXX methods
```java
StatefulLayout stateful = (StatefulLayout) findComponentById(ResourceTable.Id_stateful);
stateful.setInAnimation(new AnimatorProperty().alphaFrom(0.0f).alpha(1.0f).moveFromX(-getWindowWdith(this) * 0.5f).moveToX(0).setDuration(400));
stateful.setOutAnimation(new AnimatorProperty().alphaFrom(1.0f).alpha(0.0f).moveFromX(0).moveToX(getWindowWdith(this) * 0.5f).setDuration(400));
stateful.showLoading();
//stateful.showEmpty(getString(ResourceTable.String_testMessage));
//stateful.showError(getString(ResourceTable.String_testMessage), clickListener);
```

## API
State methods have overloads for customization. If you pass clickListener parameter as null, relevant state button 
will be hided. More customization please look [CustomStateOptions.java](https://gitee.com/HarmonyOS-tpc/StatefulLayout/blob/master/library/src/main/java/com/gturedi/views/CustomStateOptions.java)

- setAnimationEnabled(boolean animationEnabled)
- setInAnimation(AnimatorProperty animation)
- setOutAnimation(AnimatorProperty animation)
- showContent()
- showLoading(String message)
- showEmpty(String message)
- showError(String message, ClickedListener clickListener)
- showOffline(String message, ClickedListener clickListener)
- showLocationOff(String message, ClickedListener clickListener)
- showCustom(CustomStateOptions options)

## Xml Attributes
| Name | Type | Default | Description |
|:----:|:----:|:-------:|:-----------:|
| stfAnimationEnabled | boolean | true | Indicates whether to place the animation on state changes |

## Customization
Just override relevant resource in your app to customize state views appearance

- To Override **strings** [string.json variables](https://gitee.com/HarmonyOS-tpc/StatefulLayout/blob/master/library/src/main/resources/base/element/string.json)

- To Override **layout** [stf_template.xml](https://gitee.com/HarmonyOS-tpc/StatefulLayout/blob/master/library/src/main/resources/base/layout/stf_template.xml)

## entry运行要求
通过DevEco studio,并下载SDK
将项目中的build.gradle文件中dependencies→classpath版本改为对应的版本（即你的IDE新建项目中所用的版本）

